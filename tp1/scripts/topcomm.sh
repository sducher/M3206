#!/bin/bash
HISTFILE=~/.bash_history
set -o history
history | awk '{print $2}' | sort | uniq -c | sort -nr | head -$1
