#!/bin/bash
echo "[...] Checking internet connection [...]"
wget -q --tries=10 --timeout=20 --spider http://google.com
if [[ $? -eq 0 ]]; then
	echo "[...] Internet access OK [...]"
else 
	echo "[...] Not connected to Internet [...]"
fi
