#!/bin/bash

if hash git 2>/dev/null; then
	echo " Git est bien installé "
else 
	echo " lancer la commande apt-get install git pour l'installer "
fi

if hash tmux 2>/dev/null; then
	echo " tmux est bien installé "
else
	echo " lancer la commande apt-get install tmux pour l'installer "
fi

if hash vim 2>/dev/null; then
	echo " vim est bien installé "
else 
	echo " lancer la commande apt-get install vim pour l'installer "
fi

if hash htop 2>/dev/null; then
	echo " htop est bien installé "
else
	echo " lancer la commande apt-get install htop pour l'installer "
fi
