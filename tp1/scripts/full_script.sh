#Check_Internet

#!/bin/bash
echo "[...] Checking internet connection [...]"
wget -q --tries=10 --timeout=20 --spider http://google.com
if [[ $? -eq 0 ]]; then
	echo "[...] Internet access OK [...]"
else 
	echo "[...] Not connected to Internet [...]"
fi


#Chek_ssh

#!/bin/bash

dpkg -l ssh >>/dev/null 2>&1

if [ $? -eq 0 ]; then
	echo "[...] ssh : Il est bien installé [...]"
else 
	echo"[...] ssh : Il n'est pas installé [...]"
fi

ps aux|grep [s]shd >>/dev/null 2>&1 # Ici le s est entre crochet pour que le process grep ssh ne s'affiche pas

if [ $? -eq 0 ]; then
	echo "[...] ssh : Le service est bien lancé [...]"
else
	echo "[...] ssh : Le service n'est pas lancé [...]"
	echo "[...] ssh : Lancement du service [...]"
	/etc/init.d/ssh start
fi

#Chek_tools

#!/bin/bash

if hash git 2>/dev/null; then
	echo " Git est bien installé "
else 
	echo " lancer la commande apt-get install git pour l'installer "
fi

if hash tmux 2>/dev/null; then
	echo " tmux est bien installé "
else
	echo " lancer la commande apt-get install tmux pour l'installer "
fi

if hash vim 2>/dev/null; then
	echo " vim est bien installé "
else 
	echo " lancer la commande apt-get install vim pour l'installer "
fi

if hash htop 2>/dev/null; then
	echo " htop est bien installé "
else
	echo " lancer la commande apt-get install htop pour l'installer "
fi

# Update_system

#!/bin/bash
test='whoami'
if [ "$EUID" -ne 0 ]; then
	echo " Vous devez être root "
	exit
fi
apt-get update
apt-get -y dist-upgrade
echo " l'update est passé "
apt-get clean
apt-get autoremove --purge
exit 0
