#!/bin/bash

dpkg -l ssh >>/dev/null 2>&1

if [ $? -eq 0 ]; then
	echo "[...] ssh : Il est bien installé [...]"
else 
	echo"[...] ssh : Il n'est pas installé [...]"
fi

ps aux|grep [s]shd >>/dev/null 2>&1 # Ici le s est entre crochet pour que le process grep ssh ne s'affiche pas

if [ $? -eq 0 ]; then
	echo "[...] ssh : Le service est bien lancé [...]"
else
	echo "[...] ssh : Le service n'est pas lancé [...]"
	echo "[...] ssh : Lancement du service [...]"
	/etc/init.d/ssh start
fi
