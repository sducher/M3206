Exercice 1 :

- Que retourne la commande git status ? 1) 
	Cette commande retourne le status de notre GIT, donc actuellement il est sur " branch master " et il permet de dire les fichiers qui sont modifé comme par exemple là, je peut avoir en rouge que le fichier README.md est modifié

- Que retourne la commande git status ?  2)
	Ici cette commande me retourne la même chose, mais avec une autre ligne qui indique en rouge " ./ " 

- Que retourne la commande git status ? 3)
	Ici, elle me rapporte en vert " New file : TD1_Git.txt " 
	et toujours en rouge " ../README.md "

- Que retourne la commande git status ? 4)
	Lorsque j'ai tapé les commandes précédentes, il me demandais de configurer mon user name et mon email, 
je l'ai donc fait puis j'ai refais la commande
	Et ensuite pour le git status, il me ressort en rouge " modified README.md "

- Que retourne la commande git status ? 5)
	On branch master / Your branch is up-to-date with 'origin/master'. / nothing to commit, working directory clear 

EXERCICE 2 

- Que retourne la commande git log ?
	Elle me retourne tout les commit que nous avons fait depuis le début

- Que retourne la commande git status ?
	Elle m'annonce en rouge que " MODIFIED : TD1_Git.txt "

EXERCICE 3

- Que retourne la commande git status ?
	Elle ne m'annocne plus que TD1_Git.txt à était modifié 
	Et la ligne a bien disparu

EXERCICE 4 

- Que retourne la commande git status ?
	Elle m'indique en vert que README.md a bien était modifié 

- Que retourne la commande git status ?
	Elle ne m'indique plus rien en rapport avec le fichier TD1_Git_txt
