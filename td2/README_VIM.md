## Readme_VIM.md




_Exercice 1_

```bash
- jjjjjj
- i
- *temp var7 11
- echap
- jjj
- i 
- New text.
- echap
- :wq
```

_Exercice 2_

```bash
- %s/Sed/XXX/g           // On met un % afin qu'il prenne tout le texte
- %s/sed/www/g      
```

_Exercice 3_

```bash
- :4,6d
```

_Exercice 4_
 
```bash
- :s/V/i/g
- Entrée
- i
- i
```

_Méthode plus rapide en 6 touches_

```bash
- 99 
- r 
- i 
- i 
- i 
```
