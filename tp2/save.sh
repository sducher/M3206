#!/bin/bash

# Repertoire à sauvegarder 
SOURCE=$1


# Repertoire ou sera enregistré la sauvegarde
CIBLE="/tmp/backup"

# Format de la date : 
DATE=`date +%Hh%M-%d-%B-%Y`

touch $1/CHEMIN_ABSOLU.txt 
echo "$1">>$1/CHEMIN_ABSOLU.txt

NAME=`basename $SOURCE`
tar -czf $CIBLE/$NAME-$DATE.tar.gz -C $SOURCE/.. $NAME
