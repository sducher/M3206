#!/bin/bash

var=$(ls /tmp/backup/ -t | grep \.gz$ | head -1 ) 
mkdir -p /tmp/toto
cd /tmp/toto
tar -zxvf /tmp/backup/$var
mv /tmp/toto/* ~/M3206/tp2/
